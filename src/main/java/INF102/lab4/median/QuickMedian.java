package INF102.lab4.median;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class QuickMedian implements IMedian {

    private static Random random = new Random();

    @Override
    public <T extends Comparable<T>> T median(List<T> list) {
        List<T> listCopy = new ArrayList<>(list); // Method should not alter list
        int median = ((int) Math.floor((list.size()/2)));

        while (true) {            
            int size = listCopy.size();
            List<T> pivots = new ArrayList<>();
            List<T> lower = new ArrayList<>();
            List<T> higher = new ArrayList<>();
            T pivot = listCopy.get(random.nextInt(size));

            if (listCopy.size() == 1) {
                assert median == 0;
                return listCopy.get(0);
            }

            for (T e : listCopy) {
                if (e.compareTo(pivot) == -1)
                    lower.add(e);
                else if (e.compareTo(pivot) == 1)
                    higher.add(e);
                else
                    pivots.add(e);
            }

            if (median < lower.size())
                listCopy = lower;
            else if (median < lower.size() + pivots.size())
                return pivots.get(0);
            else {
                median = median - lower.size() - pivots.size();
                listCopy = higher;
            }
                
        }   
    }
}
