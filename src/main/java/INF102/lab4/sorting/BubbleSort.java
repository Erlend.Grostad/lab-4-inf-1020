package INF102.lab4.sorting;

import java.util.List;

public class BubbleSort implements ISort {

    @Override
    public <T extends Comparable<T>> void sort(List<T> list) { //O(n^2)
        int size = list.size();//O(1)
        while (true) { //O(n)
            int nsize = 0; //O(1)
            for (int i = 1; i < size; i++ ) { //O(n)
                T current = list.get(i);//O(1) potentially O(n) depending on list
                T prev = list.get(i-1);//O(1) potentially O(n) depending on list
                if (current.compareTo(prev) == -1) {//O(1)
                    list.set(i, prev);//O(1) potentially O(n) depending on list 
                    list.set(i-1, current);//O(1) potentially O(n) depending on list 
                    nsize = i;//O(1)
                }
            }
            size = nsize;//O(1)
            if (nsize <= 1) {//O(1)
                break;//O(1)
            }
        }        
    }    
}
